#!/bin/sh

# Constant
MONGO_PORT=27017
MONGOS_PORT=27018
CONFSVR_PORT=27019
DB_PORT=27020

DATA_PATH=/media/data/smartdrive/mongodb
DB_PATH=$DATA_PATH/db
LOG_PATH=$DATA_PATH/log
CONFSVR_PATH=$DATA_PATH/confsvr

###############################
# Create dependency directory #
###############################
echo "Create dependency directories "

mkdir -p $DATA_PATH
echo "  * $DATA_PATH"

mkdir -p $DB_PATH
echo "  * $DB_PATH"

mkdir -p $LOG_PATH
echo "  * $LOG_PATH"

mkdir -p $CONFSVR_PATH
echo "  * $CONFSVR_PATH"

echo DONE

#############################
# Prepare Mongo Environment #
#############################
echo "Prepare Mongo Environment"
# Kill previous pid
echo "  * Kill previous pid"
kill -9 $(lsof -t -i:$MONGO_PORT) 2> /dev/null
kill -9 $(lsof -t -i:$MONGOS_PORT) 2> /dev/null
kill -9 $(lsof -t -i:$DB_PORT) 2> /dev/null
kill -9 $(lsof -t -i:$CONFSVR_PORT) 2> /dev/null

# Show leftover mongo service
echo "  * Show leftover mongo service"
ps -ax | grep mongo

# Restart mongo service
echo "  * Restart mongo service"
service mongod --full-restart
echo DONE

echo "Noted : Start Mongo replSet & shard"
echo " 1) Start each member of the replica set with the appropriate options"
echo "      smartdrive-1# mongod -f /etc/mongod_shardsvr.conf"
echo "      smartdrive-2# mongod -f /etc/mongod_shardsvr.conf"
echo "      smartdrive-3# mongod -f /etc/mongod_shardsvr.conf"
echo " 2) Initiate the replicat set"
echo "      smartdrive-1# mongo smartdrive-1:27020/admin"
echo "      rs> rs.Initiate()"
echo "      rs> rs.add(\"smartdrive-2:27020\")"
echo "      rs> rs.add(\"smartdrive-3:27020\")"
echo " 3) Start the config server database instances"
echo "      smartdrive-1# mongod -f /etc/mongod_confsvr.conf"
echo "      smartdrive-2# mongod -f /etc/mongod_confsvr.conf"
echo "      smartdrive-3# mongod -f /etc/mongod_confsvr.conf"
echo " 4) Start the mongos instances"
echo "      smartdrive-1# mongos -f /etc/mongos_confdb.conf"
echo " 5) Run command for add shard"
echo "      smartdrive-1# mongo smartdrive-1:27017/admin"
echo "      mongos> sh.addShard(\"rs0/smartdrive-1:27020\")"
