#!/bin/sh

echo "###################"
echo "# Network Manager #"
echo "###################"

echo "  * Install NetworkManager"
sudo apt-get install network-manager

echo "  * Install the GNOME applet/indicator"
sudo apt-get install network-manager-gnome
