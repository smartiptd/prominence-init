#!/bin/sh

echo "#########"
echo "# NGiNX #"
echo "#########"

echo "  * Replace codename"
# Debian
#deb http://nginx.org/packages/debian/ codename nginx
#deb-src http://nginx.org/packages/debian/ codename nginx

# Ubuntu
deb http://nginx.org/packages/ubuntu/ codename nginx
deb-src http://nginx.org/packages/ubuntu/ codename nginx

echo "  * Install NGiNX"
apt-get update
apt-get install nginx
