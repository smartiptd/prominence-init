#!/bin/sh

PATH_DEFAULT_BIN_ZOO=/usr/local/zookeeper/zookeeper-3.4.6/bin
PATH_DEFAULT_CONF_ZOO=/usr/local/zookeeper/zookeeper-3.4.6/conf
PATH_DATA_DIR=/usr/local/project/smartdrive/zookeeper/

FILE_CONF_ZOO=../config/zookeeper/zoo.cfg
FILE_CONF_MYID=../config/zookeeper/myid

echo "############################"
echo "# Apache Zookeeper(v3.4.6) #"
echo "############################"

echo "  * Install dependency packages"
apt-get install -y aptitude
aptitude install openjdk-6-jre-headless -y

cd /tmp

echo "  * Download Zookeeper package"
wget http://www.carfab.com/apachesoftware/zookeeper/zookeeper-3.4.6/zookeeper-3.4.6.tar.gz

echo "  * Unzip package"
tar xzf zookeeper-3.4.6.tar.gz
mv zookeeper-3.4.6 /usr/local/zookeeper

echo "  * Create zookeeper config file"
cp $FILE_CONF_ZOO $PATH_DEFAULT_CONF_ZOO

echo "  * Create ID file"
cp $FILE_CONF_MYID $PATH_DATA_DIR

echo "  * Start ZooKeeper"
cd $PATH_DEFAULT_BIN_ZOO
./zkServer.sh start
