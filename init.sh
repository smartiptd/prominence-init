#!/bin/sh

# Run installation scripts
./script/nodejs.sh
./script/mongodb.sh
./script/openssl.sh
./script/zookeeper.sh
./script/rabbitmq.sh
./script/rrdtool.sh
./script/avahi-daemon.sh
./script/pm2.sh
